<?php
function tentukan_nilai($number)
{
    //  kode disini
    $n = $number;
    if($n >= 85)
    {
    	echo "Nilai ". $n. " Sangat Baik <br>";
    }
    else if($n >= 70 && $n < 85)
    {
    	echo "Nilai ". $n. " Baik <br>";
    }
    else if($n >= 60 && $n < 70)
    {
    	echo "Nilai ". $n. " Cukup <br>";
    }
    else
    {
    	echo "Nilai ". $n. " Kurang <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>